#!/usr/bin/env bash
if [ "$#" -eq "1" ]; then
  touch ./temp.txt
  server_address_variable="server.address=$1"
  echo $server_address_variable
  echo $server_address_variable >./temp.txt
  cat ./src/main/resources/application.properties | sed 1d >>./temp.txt
  cat ./temp.txt >./src/main/resources/application.properties
  echo $1 >./src/main/resources/ipAddressFile.txt
  rm ./temp.txt
fi
