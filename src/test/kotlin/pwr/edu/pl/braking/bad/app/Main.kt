package pwr.edu.pl.braking.bad.app;

import org.apache.poi.ss.usermodel.WorkbookFactory
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.FileInputStream
import java.io.FileOutputStream

fun writeToExcelFile(filepath: String) {
    val xlWb = XSSFWorkbook()
    val xlWs = xlWb.createSheet()

    xlWs.createRow(0).height = 500
    xlWs.autoSizeColumn(0)
    xlWs.getRow(0).createCell(0).setCellValue("Time")
    xlWs.getRow(0).createCell(1).setCellValue("Longitude")
    xlWs.getRow(0).createCell(2).setCellValue("Latitude")
    xlWs.getRow(0).createCell(3).setCellValue("Braking Power")
    xlWs.getRow(0).createCell(4).setCellValue("Height")
    for (i in 1..5){
        xlWs.createRow(i)
        xlWs.autoSizeColumn(i)
        for (k in 0..5){
            xlWs.getRow(i).createCell(k).setCellValue((k*10 + i).toString())
        }
    }
    val outputStream = FileOutputStream(filepath)
    xlWb.write(outputStream)
    xlWb.close()
}


fun main(args: Array<String>) {
    val filepath = "./test.xlsx"
    writeToExcelFile(filepath)
//    readFromExcelFile(filepath)
}