package pwr.edu.pl.braking.bad.app.model.nmea


class GpsSatellite(prn: Int) {
    internal var mHasEphemeris: Boolean = false
    internal var mHasAlmanac: Boolean = false
    internal var mUsedInFix: Boolean = false

    /**
     * Returns the PRN (pseudo-random number) for the satellite.
     *
     * @return PRN number
     */
    var prn: Int = 0
        internal set

    /**
     * Returns the signal to noise ratio for the satellite.
     *
     * @return the signal to noise ratio
     */
    var snr: Float = 0.toFloat()

    /**
     * Returns the elevation of the satellite in degrees.
     * The elevation can vary between 0 and 90.
     *
     * @return the elevation in degrees
     */
    var elevation: Float = 0.toFloat()

    /**
     * Returns the azimuth of the satellite in degrees.
     * The azimuth can vary between 0 and 360.
     *
     * @return the azimuth in degrees
     */
    var azimuth: Float = 0.toFloat()

    init {
        this.prn = prn
    }

    fun setHasEphemeris(hasEphemeris: Boolean) {
        this.mHasEphemeris = hasEphemeris
    }

    fun setHasAlmanac(hasAlmanac: Boolean) {
        this.mHasAlmanac = hasAlmanac
    }

    fun setUsedInFix(usedInFix: Boolean) {
        this.mUsedInFix = usedInFix
    }

    /**
     * Returns true if the GPS engine has ephemeris data for the satellite.
     *
     * @return true if the satellite has ephemeris data
     */
    fun hasEphemeris(): Boolean {
        return mHasEphemeris
    }

    /**
     * Returns true if the GPS engine has almanac data for the satellite.
     *
     * @return true if the satellite has almanac data
     */
    fun hasAlmanac(): Boolean {
        return mHasAlmanac
    }

    /**
     * Returns true if the satellite was used by the GPS engine when
     * calculating the most recent GPS fix.
     *
     * @return true if the satellite was used to compute the most recent fix.
     */
    fun usedInFix(): Boolean {
        return mUsedInFix
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val satellite = o as GpsSatellite?

        if (mHasEphemeris != satellite!!.mHasEphemeris) return false
        if (mHasAlmanac != satellite.mHasAlmanac) return false
        if (mUsedInFix != satellite.mUsedInFix) return false
        if (prn != satellite.prn) return false
        if (java.lang.Float.compare(satellite.snr, snr) != 0) return false
        return if (java.lang.Float.compare(satellite.elevation, elevation) != 0) false else java.lang.Float.compare(satellite.azimuth, azimuth) == 0

    }

    override fun hashCode(): Int {
        var result = if (mHasEphemeris) 1 else 0
        result = 31 * result + if (mHasAlmanac) 1 else 0
        result = 31 * result + if (mUsedInFix) 1 else 0
        result = 31 * result + prn
        result = 31 * result + if (snr != +0.0f) java.lang.Float.floatToIntBits(snr) else 0
        result = 31 * result + if (elevation != +0.0f) java.lang.Float.floatToIntBits(elevation) else 0
        result = 31 * result + if (azimuth != +0.0f) java.lang.Float.floatToIntBits(azimuth) else 0
        return result
    }

    override fun toString(): String {
        return "GpsSatellite{" +
                "mHasEphemeris=" + mHasEphemeris +
                ", mHasAlmanac=" + mHasAlmanac +
                ", mUsedInFix=" + mUsedInFix +
                ", mPrn=" + prn +
                ", mSnr=" + snr +
                ", mElevation=" + elevation +
                ", mAzimuth=" + azimuth +
                '}'.toString()
    }
}
