package pwr.edu.pl.braking.bad.app.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pwr.edu.pl.braking.bad.app.model.BikeLocation
import pwr.edu.pl.braking.bad.app.model.Point
import pwr.edu.pl.braking.bad.app.model.WeightRecord
import pwr.edu.pl.braking.bad.app.repository.PointRepository
import java.time.LocalDateTime


@Service
class PointService(@field:Autowired val pointRepository: PointRepository) {

    var lastWeightRecords: ArrayList<WeightRecord> = ArrayList()

    //zakladajac ze waga wysyla szybiej od gps to lista wartosci wagi odklada sie
    //w tym service i jak dojdzie kolejna lokalizacja to lista tymczasowych odczytow wagi
    // jest wrzucana do kolejnego rekordu Point i zerowana

    fun addBikeLocation(bikeLocation: BikeLocation) {
        bikeLocation.time = LocalDateTime.now()
        println(bikeLocation.toString())
        pointRepository.save(Point(this.lastWeightRecords, bikeLocation))
        lastWeightRecords = ArrayList();
    }

    fun findPointsByDay(date: LocalDateTime): ArrayList<Point> {     //dateTime probably should be beginning of a day
        val points: MutableList<Point> = pointRepository.findAll()
        val pointsInExactDay: ArrayList<Point> = ArrayList()
        val endDate = date.plusDays(1)
        for (point in points) {
            if (point.bikeLocation.time?.isAfter(date) == true) {
                if (point.bikeLocation.time?.isBefore(endDate) == true) {
                    pointsInExactDay.add(point)
                }
            }
        }
        println(date.toString() + " has " + pointsInExactDay.size + " Points")
        return pointsInExactDay
    }

}
