package pwr.edu.pl.braking.bad.app.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pwr.edu.pl.braking.bad.app.model.WeightRecord
import pwr.edu.pl.braking.bad.app.repository.WeightRepository
import java.time.LocalDateTime

@Service
class WeightRecordService(weightsRepository: WeightRepository, locationService: PointService) {

    @Autowired
    val weightRepository = weightsRepository

    @Autowired
    val locationService = locationService

    fun createWeightRecord(weight: Float): WeightRecord {
        val time = LocalDateTime.now()
        return WeightRecord(weight, time)
    }

    fun addNewWeightRecordToRepositoryAndPointService(weight: Float) {
        val weightRecord = createWeightRecord(weight)
        weightRepository.save(weightRecord)
        println(weightRecord.toString())
        addWeightToPointsList(weight)
    }

    private fun addWeightToPointsList(weight: Float) {
        locationService.lastWeightRecords.add(createWeightRecord(weight))
    }
}