package pwr.edu.pl.braking.bad.app.repository

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import pwr.edu.pl.braking.bad.app.model.Signal

@Repository
interface SignalRepository : MongoRepository<Signal, String> {
    override fun findAll(): MutableList<Signal>
}