package pwr.edu.pl.braking.bad.app.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pwr.edu.pl.braking.bad.app.model.Signal
import pwr.edu.pl.braking.bad.app.repository.SignalRepository
import java.time.LocalDateTime

@Service
class SignalService(@field:Autowired val signalRepository: SignalRepository) {

    fun saveSignal(isStopSignal: Boolean) {
        val signal = Signal(isStopSignal)
        signal.time = LocalDateTime.now()
        this.signalRepository.save(signal)
        //@ToDo sprawdzic czy poprzedni otrzymany sygnał byl przeciwny do aktualnego (start->stop)
    }

    fun findSignalByDate(date: LocalDateTime): ArrayList<Signal> {
        val signals: MutableList<Signal> = signalRepository.findAll()
        val signalsInExactDay: ArrayList<Signal> = ArrayList()
        val endDate = date.plusDays(1)
        for (signal in signals) {
            if (signal.time?.isAfter(date) == true) {
                if (signal.time?.isBefore(endDate) == true) {
                    signalsInExactDay.add(signal)
                }
            }
        }
        return signalsInExactDay
    }
}