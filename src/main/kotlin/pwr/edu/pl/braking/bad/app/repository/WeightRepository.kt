package pwr.edu.pl.braking.bad.app.repository

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import pwr.edu.pl.braking.bad.app.model.WeightRecord

@Repository
interface WeightRepository : MongoRepository<WeightRecord, String> {
//fun findByTime(time:LocalDate):WeightRecord?
}