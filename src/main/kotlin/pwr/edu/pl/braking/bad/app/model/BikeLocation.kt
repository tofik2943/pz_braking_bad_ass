package pwr.edu.pl.braking.bad.app.model

import java.time.LocalDateTime

class BikeLocation(private val vDir: String //N or S
                   , private val hDir: String // E or W
                   , val latitude: String, val longitude: String, public val altitude: String) {

    var time: LocalDateTime? = null
    override fun toString(): String {
        return "@@@@@@@@@@@@@@@_LOCATION_@@@@@@@@@@@@@@@@@@@@@\n" +
                "longitude='$longitude''$vDir,\n" +
                "latitude='$latitude''$hDir',\n" +
                "altitude='$altitude',\n" +
                "time=$time\n" +
                "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
    }

}
