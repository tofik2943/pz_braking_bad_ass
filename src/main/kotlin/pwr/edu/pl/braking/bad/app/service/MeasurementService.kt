package pwr.edu.pl.braking.bad.app.service

import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pwr.edu.pl.braking.bad.app.model.BikeLocation
import pwr.edu.pl.braking.bad.app.model.Measurement
import pwr.edu.pl.braking.bad.app.model.Point
import pwr.edu.pl.braking.bad.app.model.Signal
import pwr.edu.pl.braking.bad.app.model.exceptions.ContinuityException
import java.io.FileOutputStream
import java.time.LocalDateTime

@Service
class MeasurementService(@field:Autowired val pointService: PointService,
                         @field:Autowired val signalService: SignalService) {

    fun createMeasurements(date: LocalDateTime): ArrayList<Measurement> {
        val measurements: ArrayList<Measurement> = ArrayList()
        val points = pointService.findPointsByDay(date)
        val signals = signalService.findSignalByDate(date)
        var startSignal: Signal? = null
        var stopSignal: Signal? = null
        for (i in 0 until signals.size) {
            if (!signals[i].isStopSignal) {
                startSignal = signals[i]
                if (stopSignal != null) {
                    throw ContinuityException()
                }
            } else {
                stopSignal = signals[i]
                if (startSignal == null) {
                    throw ContinuityException()
                }
                val pointsBetween = huskPointsBetweenStartAndStop(startSignal, stopSignal, points)
                points.removeAll(pointsBetween)
                measurements.add(createSingleMeasurement(startSignal, stopSignal, pointsBetween))
                startSignal = null
                stopSignal = null
            }
        }
        return measurements
    }

    fun huskPointsBetweenStartAndStop(startSignal: Signal, stopSignal: Signal, points: ArrayList<Point>): ArrayList<Point> {
        val pointsBetween: ArrayList<Point> = ArrayList()
        for (point in points) {
            if (point.bikeLocation.time?.isAfter(startSignal.time) == true) {
                if (point.bikeLocation.time?.isBefore(stopSignal.time) == true) {
                    pointsBetween.add(point)
                }
            }
        }
        return pointsBetween
    }

    fun createSingleMeasurement(startSignal: Signal, stopSignal: Signal, points: ArrayList<Point>): Measurement {
        return Measurement(points, startSignal, stopSignal)
    }

    fun writeToExcelFile(filepath: String, date: LocalDateTime) {
        val points = pointService.findPointsByDay(date)
        val xlWb = XSSFWorkbook()
        val xlWs = xlWb.createSheet()
        xlWs.createRow(0)
        xlWs.autoSizeColumn(0)
        xlWs.getRow(0).createCell(0).setCellValue("Time")
        xlWs.getRow(0).createCell(1).setCellValue("Longitude")
        xlWs.getRow(0).createCell(2).setCellValue("Latitude")
        xlWs.getRow(0).createCell(3).setCellValue("Braking Power")
        xlWs.getRow(0).createCell(4).setCellValue("Altitude")
        var lastAvgWeightValue = 0.0

        for ((point_num, point) in points.withIndex()) {
            val rowNum = point_num + 1
            xlWs.createRow(rowNum)
            xlWs.autoSizeColumn(rowNum)
            val location: BikeLocation = point.bikeLocation
            xlWs.getRow(rowNum).createCell(0).setCellValue(location.time.toString())
            xlWs.getRow(rowNum).createCell(1).setCellValue(location.longitude)
            xlWs.getRow(rowNum).createCell(2).setCellValue(location.latitude)
            var avgWeightValue: Double? = points[point_num].countAverageWeightValue()
            if (avgWeightValue == null) {
                avgWeightValue = lastAvgWeightValue
            } else {
                lastAvgWeightValue = avgWeightValue
            }
            xlWs.getRow(rowNum).createCell(3).setCellValue(avgWeightValue)
            xlWs.getRow(rowNum).createCell(4).setCellValue(location.altitude)
        }
        val outputStream = FileOutputStream(filepath)
        xlWb.write(outputStream)
        xlWb.close()
    }

    fun generateStringCSV(date: LocalDateTime): String {
        var weightRecordsCount: Int = 0
        val measurements: ArrayList<Measurement> = this.createMeasurements(date)
        val stringBufferCSV = StringBuffer("")
        for (measurement in measurements) {
            val points = measurement.points
            for (point in points) {
                val weightRecords = point.weightRecords
                weightRecordsCount += weightRecords.size
                for (weight in weightRecords) {
                    stringBufferCSV.append(weight.weightValue)
                    stringBufferCSV.append(",")
                }
            }
            stringBufferCSV.append("\n")
            println("Registered " + weightRecordsCount + "weight records in measurement")
            weightRecordsCount = 0
        }
        writeToExcelFile("./braking_bad.xlsx",date)
        return stringBufferCSV.toString()
    }
}
