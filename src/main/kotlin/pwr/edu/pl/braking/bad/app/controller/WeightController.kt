package pwr.edu.pl.braking.bad.app.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pwr.edu.pl.braking.bad.app.service.PointService
import pwr.edu.pl.braking.bad.app.service.WeightRecordService


@RestController
@RequestMapping("/weight")
class WeightController(weightRecordService: WeightRecordService, locationService: PointService) {

    @Autowired
    val weightRecordService: WeightRecordService = weightRecordService

    @Autowired
    val locationServiceService: PointService = locationService

    @PostMapping
    fun addWeightRecord(weightValue: Float?) {
        if (weightValue != null) {
            weightRecordService.addNewWeightRecordToRepositoryAndPointService(weightValue)
        }
    }
}
