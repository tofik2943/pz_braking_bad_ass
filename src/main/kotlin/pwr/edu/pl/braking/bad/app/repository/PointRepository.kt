package pwr.edu.pl.braking.bad.app.repository

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import pwr.edu.pl.braking.bad.app.model.Point

@Repository
interface PointRepository : MongoRepository<Point, String> {
    override fun findAll(): MutableList<Point>
}