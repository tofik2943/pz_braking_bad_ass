package pwr.edu.pl.braking.bad.app.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document(collection = "weightRecords")
class WeightRecord(var weightValue: Float?, var time: LocalDateTime?) {

    @Id
    var id: String? = null
    override fun toString(): String {
        return "#################_WEIGHT_######################\n" +
                "weightValue=$weightValue,\n" +
                "time=$time)\n" +
                "##############################################\n"
    }


}