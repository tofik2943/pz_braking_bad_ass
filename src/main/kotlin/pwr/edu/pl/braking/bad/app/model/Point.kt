package pwr.edu.pl.braking.bad.app.model

import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "points")
class Point(var weightRecords: ArrayList<WeightRecord>, val bikeLocation: BikeLocation) {
    fun countAverageWeightValue(): Double? {
        var average: Double = 0.0
        if (weightRecords.size == 0) {
            return null
        }
        for (weight in weightRecords) {
            average += weight.weightValue!!
        }
        return average/weightRecords.size
    }
}

