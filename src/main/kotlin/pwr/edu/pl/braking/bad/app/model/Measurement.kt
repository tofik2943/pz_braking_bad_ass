package pwr.edu.pl.braking.bad.app.model

class Measurement(var points: ArrayList<Point>, val startSignal: Signal, val stopSignal: Signal)
