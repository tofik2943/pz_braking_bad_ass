package pwr.edu.pl.braking.bad.app.udp


import android.location.Location
import pwr.edu.pl.braking.bad.app.model.nmea.GpsSatellite
import pwr.edu.pl.braking.bad.app.model.nmea.NMEAHandler
import pwr.edu.pl.braking.bad.app.model.nmea.NMEAParser
import pwr.edu.pl.braking.bad.app.service.PointService
import java.io.IOException
import java.net.*


class UDPServer(private val port: Int, private val serverAddress: String, val locationService: PointService) : Thread() {

    private val parser: NMEAParser
    var socket: DatagramSocket? = null

    private val running: Boolean = false
    private val buf = ByteArray(82)
    internal var counter = 0
    lateinit var handler: NMEAHandler

    init {
        open()
        createHandler()
        this.parser = NMEAParser(this.handler, locationService)
    }

    fun open() {
        try {
            socket = DatagramSocket(port, InetAddress.getByName(serverAddress))
        } catch (e: SocketException) {
            e.printStackTrace()
        } catch (e: UnknownHostException) {
            e.printStackTrace()
        }

    }

    override fun run() {
        while (true) {
            try {
                var packet = DatagramPacket(buf, buf.size)
                socket!!.receive(packet)
                val address = packet.address
                val port = packet.port
                packet = DatagramPacket(buf, buf.size, address, port)
                var received = String(packet.data, 0, packet.length)

                run {
                    //					System.out.println("\nMessage " + counter++ + ":");
                    //					System.out.println(received);
                    //					System.out.println("///////////////////////////////////////////");
                    received = trim(received)
                    //					System.out.println("/////////////////TRIMMED///////////////////");
                    //					System.out.println(received);
                    //					System.out.println("///////////////////////////////////////////");
                    parser.parse(received)
                }

            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    fun trim(received: String): String {
        var received = received
        received = received.replace("\\r\\n(.*)".toRegex(), "").trim { it <= ' ' }
        //received = received.replaceAll("(0{3,})(.*)", "").trim();
        return received
    }

    fun createHandler() {
        this.handler = object : NMEAHandler {

            override fun onStart() {

            }

            override fun onLocation(location: Location) {
                println(location.accuracy)
            }

            override fun onSatellites(satellites: List<GpsSatellite>) {

            }

            override fun onUnrecognized(sentence: String) {

            }

            override fun onBadChecksum(expected: Int, actual: Int) {

            }

            override fun onException(e: Exception) {

            }

            override fun onFinish() {

            }
        }
    }
}
