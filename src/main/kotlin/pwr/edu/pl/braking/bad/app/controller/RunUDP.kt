package pwr.edu.pl.braking.bad.app.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pwr.edu.pl.braking.bad.app.service.PointService
import pwr.edu.pl.braking.bad.app.udp.UDPServer
import java.io.File
import java.nio.file.FileSystems
import java.nio.file.Paths


@RestController
@RequestMapping("/start")
class
RunUDP(locationService: PointService) {

    @Autowired
    val locationServiceService: PointService = locationService
    var ipAddress: String? = null

    @PostMapping
    fun startNmeaServer(weightValue: Float?) {
        startUdpServer()
    }

    fun readIpAddress(): String {
        var fs = FileSystems.getDefault().getSeparator()
        val projectRootPath = System.getProperty("user.dir")
        var filePath: String = Paths.get("${projectRootPath}${fs}src${fs}main${fs}resources${fs}ipAddressFile.txt").toString()
        return File(filePath).bufferedReader().readLines()[0]

    }

    fun startUdpServer() {
        val port = 59048
        this.ipAddress = readIpAddress()
        val udpServer = UDPServer(port, ipAddress!!, locationServiceService)
        println("UDP SERVER STARTED")
        udpServer.start()
    }
}
