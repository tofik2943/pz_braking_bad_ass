package pwr.edu.pl.braking.bad.app

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories


//@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@EnableMongoRepositories("pwr.edu.pl.braking.bad.app.repository")
@ComponentScan(basePackages = arrayOf("pwr.edu.pl.braking.bad.app"))
class AppApplication

fun main(args: Array<String>) {
    runApplication<AppApplication>(*args)
}
