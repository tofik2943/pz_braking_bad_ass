package pwr.edu.pl.braking.bad.app.model.exceptions

class ContinuityException(message: String = "(Start -> Stop) continuity was violated")
    : Exception(message) {
}