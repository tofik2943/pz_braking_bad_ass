package pwr.edu.pl.braking.bad.app.model.nmea.basic


import pwr.edu.pl.braking.bad.app.model.BikeLocation
import pwr.edu.pl.braking.bad.app.service.PointService

import java.io.UnsupportedEncodingException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


class BasicNMEAParser(private val handler: BasicNMEAHandler?, private val locationService: PointService) {

    init {
        TIME_FORMAT.timeZone = TimeZone.getTimeZone("UTC")
        DATE_FORMAT.timeZone = TimeZone.getTimeZone("UTC")
        functions["GPRMC"] = object : ParsingFunction() {
            @Throws(Exception::class)
            override fun parse(handler: BasicNMEAHandler, sentence: String?): Boolean {
                return parseGPRMC(handler, sentence)
            }
        }
        functions["GPGGA"] = object : ParsingFunction() {
            @Throws(Exception::class)
            override fun parse(handler: BasicNMEAHandler, sentence: String?): Boolean {
                return parseGPGGA(handler, sentence)
            }
        }
        functions["GPGSV"] = object : ParsingFunction() {
            @Throws(Exception::class)
            override fun parse(handler: BasicNMEAHandler, sentence: String?): Boolean {
                return parseGPGSV(handler, sentence)
            }
        }
        functions["GPGSA"] = object : ParsingFunction() {
            @Throws(Exception::class)
            override fun parse(handler: BasicNMEAHandler, sentence: String?): Boolean {
                return parseGPGSA(handler, sentence)
            }
        }
    }

    init {
        if (handler == null) {
            throw NullPointerException()
        }
    }

    @Throws(Exception::class)
    private fun parseGPGGA(handler: BasicNMEAHandler, sentence: String?): Boolean {
        val matcher = ExMatcher(GPGGA.matcher(sentence!!))
        if (matcher.matches()) {
            var time = TIME_FORMAT.parse(matcher.nextString("time")!! + "0").time
            val ms = matcher.nextFloat("time-ms")
            if (ms != null) {
                time += (ms * 1000).toLong()
            }
            val latitude = toDegrees(matcher.nextInt("degrees")!!,
                    matcher.nextFloat("minutes")!!)
            val vDir = VDir.valueOf(matcher.nextString("vertical-direction")!!)
            val longitude = toDegrees(matcher.nextInt("degrees")!!,
                    matcher.nextFloat("minutes")!!)
            val hDir = HDir.valueOf(matcher.nextString("horizontal-direction")!!)
            val quality = BasicNMEAHandler.FixQuality.values()[matcher.nextInt("quality")!!]
            val satellites = matcher.nextInt("n-satellites")!!
            val hdop = matcher.nextFloat("hdop")!!
            val altitude = matcher.nextFloat("altitude")!!
            val separation = matcher.nextFloat("separation")!!
            val age = matcher.nextFloat("age")
            val station = matcher.nextInt("station")
            locationService.addBikeLocation(
                    BikeLocation(vDir.toString(),
                            hDir.toString(),
                            latitude.toString(),
                            longitude.toString(),
                            altitude.toString()))
            handler.onGGA(time,
                    if (vDir == VDir.N) latitude else -latitude,
                    if (hDir == HDir.E) longitude else -longitude,
                    altitude - separation,
                    quality,
                    satellites,
                    hdop)

            return true
        }

        return false
    }

    @Throws(Exception::class)
    private fun parseGPGSV(handler: BasicNMEAHandler, sentence: String?): Boolean {
        val matcher = ExMatcher(GPGSV.matcher(sentence!!))
        if (matcher.matches()) {
            val sentences = matcher.nextInt("n-sentences")!!
            val index = matcher.nextInt("sentence-index")!! - 1
            val satellites = matcher.nextInt("n-satellites")!!

            for (i in 0..3) {
                val prn = matcher.nextInt("prn")
                val elevation = matcher.nextInt("elevation")
                val azimuth = matcher.nextInt("azimuth")
                val snr = matcher.nextInt("snr")

                if (prn != null) {
                    handler.onGSV(satellites, index * 4 + i, prn, elevation!!.toFloat(), azimuth!!.toFloat(), snr!!)
                }
            }

            return true
        }
        return false
    }

    @Synchronized
    fun parse(sentence: String?) {
        if (sentence == null) {
            throw NullPointerException()
        }

        handler!!.onStart()
        try {
            val matcher = ExMatcher(GENERAL_SENTENCE.matcher(sentence))
            if (matcher.matches()) {
                val type = matcher.nextString("type")
                val content = matcher.nextString("content")
                val expected_checksum = matcher.nextHexInt("checksum")!!
                val actual_checksum = calculateChecksum(sentence)

                if (actual_checksum != expected_checksum) {
                    handler.onBadChecksum(expected_checksum, actual_checksum)
                } else if (!functions.containsKey(type) || !functions[type]?.parse(handler, content)!!) {
                    handler.onUnrecognized(sentence)
                }
            } else {
                handler.onUnrecognized(sentence)
            }
        } catch (e: Exception) {
            handler.onException(e)
        } finally {
            handler.onFinished()
        }
    }

    private enum class Status {
        A,
        V
    }

    private enum class HDir {
        E,
        W
    }

    private enum class VDir {
        N,
        S
    }

    private enum class Mode {
        A,
        M
    }

    private enum class FFA {
        A,
        D,
        E,
        M,
        S,
        N
    }

    private abstract class ParsingFunction {
        @Throws(Exception::class)
        abstract fun parse(handler: BasicNMEAHandler, sentence: String?): Boolean
    }

    private class ExMatcher internal constructor(internal var original: Matcher) {
        internal var index: Int = 0

        init {
            reset()
        }

        internal fun reset() {
            index = 1
        }

        internal fun matches(): Boolean {
            return original.matches()
        }

        internal fun nextString(name: String): String? {
            return original.group(index++)
        }

        internal fun nextFloat(name: String, defaultValue: Float?): Float? {
            val next = nextFloat(name)
            return next ?: defaultValue
        }

        internal fun nextFloat(name: String): Float? {
            val next = nextString(name)
            return if (next == null) null else java.lang.Float.parseFloat(next)
        }

        internal fun nextInt(name: String): Int? {
            val next = nextString(name)
            return if (next == null) null else Integer.parseInt(next)
        }

        internal fun nextHexInt(name: String): Int? {
            val next = nextString(name)
            return if (next == null) null else Integer.parseInt(next, 16)
        }
    }

    companion object {
        private val KNOTS2MPS = 0.514444f
        private val TIME_FORMAT = SimpleDateFormat("HHmmss", Locale.US)
        private val DATE_FORMAT = SimpleDateFormat("ddMMyy", Locale.US)
        private val COMMA = ","
        private val CAP_FLOAT = "(\\d*[.]?\\d+)"
        private val HEX_INT = "[0-9a-fA-F]"
        private val GENERAL_SENTENCE = Pattern.compile("^\\$(\\w{5}),(.*)[*]($HEX_INT{2})$")
        private val GPRMC = Pattern.compile("(\\d{5})?" +
                "(\\d[.]?\\d*)?" + COMMA +
                regexify(Status::class.java) + COMMA +
                "(\\d{2})(\\d{2}[.]\\d+)?" + COMMA +
                regexify(VDir::class.java) + "?" + COMMA +
                "(\\d{3})(\\d{2}[.]\\d+)?" + COMMA +
                regexify(HDir::class.java) + "?" + COMMA +
                CAP_FLOAT + "?" + COMMA +
                CAP_FLOAT + "?" + COMMA +
                "(\\d{6})?" + COMMA +
                CAP_FLOAT + "?" + COMMA +
                regexify(HDir::class.java) + "?" + COMMA + "?" +
                regexify(FFA::class.java) + "?")
        private val GPGGA = Pattern.compile("(\\d{5})?" +
                "(\\d[.]?\\d*)?" + COMMA +
                "(\\d{2})(\\d{2}[.]\\d+)?" + COMMA +
                regexify(VDir::class.java) + "?" + COMMA +
                "(\\d{3})(\\d{2}[.]\\d+)?" + COMMA +
                regexify(HDir::class.java) + "?" + COMMA +
                "(\\d)?" + COMMA +
                "(\\d{2})?" + COMMA +
                CAP_FLOAT + "?" + COMMA +
                CAP_FLOAT + "?,[M]" + COMMA +
                CAP_FLOAT + "?,[M]" + COMMA +
                CAP_FLOAT + "?" + COMMA +
                "(\\d{4})?")
        private val GPGSV = Pattern.compile("(\\d+)" + COMMA +
                "(\\d+)" + COMMA +
                "(\\d{2})" + COMMA +

                "(\\d{2})" + COMMA +
                "(\\d{2})" + COMMA +
                "(\\d{3})" + COMMA +
                "(\\d{2})" + COMMA +

                "(\\d{2})?" + COMMA + "?" +
                "(\\d{2})?" + COMMA + "?" +
                "(\\d{3})?" + COMMA + "?" +
                "(\\d{2})?" + COMMA + "?" +

                "(\\d{2})?" + COMMA + "?" +
                "(\\d{2})?" + COMMA + "?" +
                "(\\d{3})?" + COMMA + "?" +
                "(\\d{2})?" + COMMA + "?" +

                "(\\d{2})?" + COMMA + "?" +
                "(\\d{2})?" + COMMA + "?" +
                "(\\d{3})?" + COMMA + "?" +
                "(\\d{2})?")
        private val GPGSA = Pattern.compile(regexify(Mode::class.java) + COMMA +
                "(\\d)" + COMMA +

                "(\\d{2})?" + COMMA +
                "(\\d{2})?" + COMMA +
                "(\\d{2})?" + COMMA +
                "(\\d{2})?" + COMMA +
                "(\\d{2})?" + COMMA +
                "(\\d{2})?" + COMMA +
                "(\\d{2})?" + COMMA +
                "(\\d{2})?" + COMMA +
                "(\\d{2})?" + COMMA +
                "(\\d{2})?" + COMMA +
                "(\\d{2})?" + COMMA +
                "(\\d{2})?" + COMMA +

                CAP_FLOAT + "?" + COMMA +
                CAP_FLOAT + "?" + COMMA +
                CAP_FLOAT + "?")
        private val functions = HashMap<String, ParsingFunction>()

        @Throws(Exception::class)
        private fun parseGPRMC(handler: BasicNMEAHandler, sentence: String?): Boolean {
            val matcher = ExMatcher(GPRMC.matcher(sentence!!))
            if (matcher.matches()) {
                var time = TIME_FORMAT.parse(matcher.nextString("time")!! + "0").time
                val ms = matcher.nextFloat("time-ms")
                if (ms != null) {
                    time += (ms * 1000).toLong()
                }
                if (Status.valueOf(matcher.nextString("status")!!) == Status.A) {
                    val latitude = toDegrees(matcher.nextInt("degrees")!!,
                            matcher.nextFloat("minutes")!!)
                    val vDir = VDir.valueOf(matcher.nextString("vertical-direction")!!)
                    val longitude = toDegrees(matcher.nextInt("degrees")!!,
                            matcher.nextFloat("minutes")!!)
                    val hDir = HDir.valueOf(matcher.nextString("horizontal-direction")!!)
                    val speed = matcher.nextFloat("speed")!! * KNOTS2MPS
                    val direction = matcher.nextFloat("direction", 0.0f)!!
                    val date = DATE_FORMAT.parse(matcher.nextString("date")).time
                    val magVar = matcher.nextFloat("magnetic-variation")
                    val magVarDir = matcher.nextString("direction")
                    val faa = matcher.nextString("faa")

                    handler.onRMC(date,
                            time,
                            if (vDir == VDir.N) latitude else -latitude,
                            if (hDir == HDir.E) longitude else -longitude,
                            speed,
                            direction)

                    return true
                }
            }

            return false
        }

        private fun parseGPGSA(handler: BasicNMEAHandler, sentence: String?): Boolean {
            val matcher = ExMatcher(GPGSA.matcher(sentence!!))
            if (matcher.matches()) {
                val mode = matcher.nextString("mode")?.let { Mode.valueOf(it) }
                val type = BasicNMEAHandler.FixType.values()[matcher.nextInt("fix-type")!!]
                val prns = HashSet<Int>()
                for (i in 0..11) {
                    val prn = matcher.nextInt("prn")
                    if (prn != null) {
                        prns.add(prn)
                    }
                }
                val pdop = matcher.nextFloat("pdop")!!
                val hdop = matcher.nextFloat("hdop")!!
                val vdop = matcher.nextFloat("vdop")!!

                handler.onGSA(type, prns, pdop, hdop, vdop)

                return true
            }
            return false
        }

        @Throws(UnsupportedEncodingException::class)
        private fun calculateChecksum(sentence: String): Int {
            val bytes = sentence.substring(1, sentence.length - 3).toByteArray(charset("US-ASCII"))
            var checksum = 0
            for (b in bytes) {
                checksum = checksum xor b.toInt()
            }
            return checksum
        }

        private fun toDegrees(degrees: Int, minutes: Float): Double {
            return degrees + minutes / 60.0
        }

        private fun <T : Enum<T>> regexify(clazz: Class<T>): String {
            val sb = StringBuilder()
            sb.append("([")
            for (c in clazz.enumConstants) {
                sb.append(c.toString())
            }
            sb.append("])")

            return sb.toString()
        }
    }
}
