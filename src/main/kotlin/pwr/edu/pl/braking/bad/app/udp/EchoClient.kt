package pwr.edu.pl.braking.bad.app.udp

import java.io.IOException
import java.net.*

class EchoClient(private val port: Int, private val serverAddress: String) {
    private var socket: DatagramSocket? = null
    private var address: InetAddress? = null
    private var buf: ByteArray? = null

    init {
        try {
            socket = DatagramSocket()
            println(serverAddress)
            address = InetAddress.getByName(serverAddress)
        } catch (e: UnknownHostException) {
            e.printStackTrace()
        } catch (e: SocketException) {
            e.printStackTrace()
        }

    }

    fun sendString(msg: String) {
        try {
            buf = msg.toByteArray()
            val packet = DatagramPacket(buf!!, buf!!.size, address, port)
            socket!!.send(packet)
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    fun close() {
        socket!!.close()
    }
}
