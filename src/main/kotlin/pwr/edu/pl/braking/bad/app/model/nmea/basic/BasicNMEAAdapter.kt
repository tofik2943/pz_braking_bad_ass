package pwr.edu.pl.braking.bad.app.model.nmea.basic

class BasicNMEAAdapter : BasicNMEAHandler {
    override fun onStart() {

    }

    override fun onRMC(date: Long, time: Long, latitude: Double, longitude: Double, speed: Float, direction: Float) {

    }

    override fun onGGA(time: Long, latitude: Double, longitude: Double, altitude: Float, quality: BasicNMEAHandler.FixQuality, satellites: Int, hdop: Float) {

    }

    override fun onGSV(satellites: Int, index: Int, prn: Int, elevation: Float, azimuth: Float, snr: Int) {

    }

    override fun onGSA(type: BasicNMEAHandler.FixType, prns: Set<Int>, pdop: Float, hdop: Float, vdop: Float) {

    }

    override fun onUnrecognized(sentence: String) {

    }

    override fun onBadChecksum(expected: Int, actual: Int) {

    }

    override fun onException(e: Exception) {

    }

    override fun onFinished() {

    }
}
