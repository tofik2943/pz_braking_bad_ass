package pwr.edu.pl.braking.bad.app.model.nmea.basic

interface BasicNMEAHandler {
    fun onStart()

    /***
     * Called on GPRMC parsed.
     *
     * @param date      milliseconds since midnight, January 1, 1970 UTC.
     * @param time      actual UTC time (without date)
     * @param latitude  angular y position on the Earth.
     * @param longitude angular x position on the Earth.
     * @param speed     in meters per second.
     * @param direction angular bearing value to the North.
     */
    fun onRMC(date: Long, time: Long, latitude: Double, longitude: Double, speed: Float, direction: Float)

    /***
     * Called on GPGGA parsed.
     *
     * @param time        actual UTC time (without date)
     * @param latitude    angular y position on the Earth.
     * @param longitude   angular x position on the Earth.
     * @param altitude    altitude in meters above corrected geoid
     * @param quality     fix-quality type [FixQuality]
     * @param satellites  actual number of satellites
     * @param hdop        horizontal dilution of precision
     */
    fun onGGA(time: Long, latitude: Double, longitude: Double, altitude: Float, quality: FixQuality, satellites: Int, hdop: Float)

    /***
     * Called on GPGSV parsed.
     * Note that single nmea sentence contains up to 4 satellites therefore you can receive 4 calls per sentence.
     *
     * @param satellites total number of satellites
     * @param index      index of satellite
     * @param prn        pseudo-random noise number
     * @param elevation  elevation in degrees
     * @param azimuth    azimuth in degrees
     * @param snr        signal to noise ratio
     */
    fun onGSV(satellites: Int, index: Int, prn: Int, elevation: Float, azimuth: Float, snr: Int)

    /***
     * Called on GPGSA parsed.
     *
     * @param type type of fix
     * @param prns set of satellites used for the current fix
     * @param pdop position dilution of precision
     * @param hdop horizontal dilution of precision
     * @param vdop vertical dilution of precision
     */
    fun onGSA(type: FixType, prns: Set<Int>, pdop: Float, hdop: Float, vdop: Float)

    fun onUnrecognized(sentence: String)

    fun onBadChecksum(expected: Int, actual: Int)

    fun onException(e: Exception)

    fun onFinished()

    enum class FixQuality private constructor(val value: Int) {
        Invalid(0),
        GPS(1),
        DGPS(2),
        PPS(3),
        IRTK(4),
        FRTK(5),
        Estimated(6),
        Manual(7),
        Simulation(8)
    }

    enum class FixType private constructor(val value: Int) {
        Invalid(0),
        None(1),
        Fix2D(2),
        Fix3D(3)
    }
}
