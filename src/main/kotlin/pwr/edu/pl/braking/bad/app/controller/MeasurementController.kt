package pwr.edu.pl.braking.bad.app.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pwr.edu.pl.braking.bad.app.service.MeasurementService
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


@RestController
@RequestMapping("/measurement")
class MeasurementController(@field:Autowired val measurementService: MeasurementService) {

    @GetMapping
    fun retrieveMeasurements(dateTimeInString: String): String {
        val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")
        val date = LocalDateTime.parse(dateTimeInString, formatter)
        return measurementService.generateStringCSV(date)
    }
}