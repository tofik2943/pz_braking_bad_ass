package pwr.edu.pl.braking.bad.app.model

import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document(collection = "signals")
class Signal(var isStopSignal: Boolean) {
    var time: LocalDateTime? = null
}
