package pwr.edu.pl.braking.bad.app.model.nmea

import android.location.Location
import pwr.edu.pl.braking.bad.app.model.nmea.basic.BasicNMEAHandler
import pwr.edu.pl.braking.bad.app.model.nmea.basic.BasicNMEAParser
import pwr.edu.pl.braking.bad.app.service.PointService
import java.util.*

class NMEAParser(private val handler: NMEAHandler?, private val locationFactory: LocationFactory, locationService: PointService) : BasicNMEAHandler {
    private val basicParser: BasicNMEAParser
    private var location: Location? = null
    internal var locationService: PointService? = null
    private var lastTime: Long = 0
    private var flags: Int = 0
    private var satellitesCount: Int = 0
    private val tempSatellites = arrayOfNulls<GpsSatellite>(SATELLITES_COUNT)
    private var activeSatellites: Set<Int>? = null

    constructor(handler: NMEAHandler, locationService: PointService) : this(handler, object : LocationFactory() {
        override fun newLocation(): Location {
            return Location(LOCATION_PROVIDER_NAME)
        }
    }, locationService) {
    }

    init {
        basicParser = BasicNMEAParser(this, locationService)

        if (handler == null) {
            throw NullPointerException()
        }
    }

    @Synchronized
    fun parse(sentence: String) {
        basicParser.parse(sentence)
    }

    private fun resetLocationState() {
        flags = 0
        lastTime = 0
    }

    private fun newLocation(time: Long) {
        if (location == null || time != lastTime) {
            location = locationFactory.newLocation()
            resetLocationState()
        }
    }

    private fun yieldLocation(time: Long, flag: Int) {
        if (flags or (flag and LOCATION_FLAGS) == LOCATION_FLAGS) {
            handler!!.onLocation(location!!)
            resetLocationState()
        } else {
            flags = flags or flag
            lastTime = time
        }
    }

    private fun hasAllSatellites(): Boolean {
        for (i in 0 until satellitesCount) {
            if (tempSatellites[i] == null) {
                return false
            }
        }

        return true
    }

    private fun yieldSatellites() {
        if (satellitesCount > 0 && hasAllSatellites() && activeSatellites != null) {
            for (satellite in tempSatellites) {
                if (satellite == null) {
                    break
                } else {
                    satellite.setUsedInFix(activeSatellites!!.contains(satellite.prn))
                    satellite.setHasAlmanac(true) // TODO: ...
                    satellite.setHasEphemeris(true)  // TODO: ...
                }
            }

            handler!!.onSatellites(Arrays.asList(*Arrays.copyOf(tempSatellites, satellitesCount)))

            Arrays.fill(tempSatellites, null)
            activeSatellites = null
            satellitesCount = 0
        }
    }

    private fun newSatellite(index: Int, count: Int, prn: Int, elevation: Float, azimuth: Float, snr: Int) {
        if (count != satellitesCount) {
            satellitesCount = count
        }

        val satellite = GpsSatellite(prn)
        satellite.azimuth = azimuth
        satellite.elevation = elevation
        satellite.snr = snr.toFloat()

        tempSatellites[index] = satellite
    }

    @Synchronized
    override fun onStart() {
        handler!!.onStart()
    }

    @Synchronized
    override fun onRMC(date: Long, time: Long, latitude: Double, longitude: Double, speed: Float, direction: Float) {
        newLocation(time)

        location!!.time = date or time
        location!!.speed = speed
        location!!.bearing = direction

        yieldLocation(time, FLAG_RMC)
    }

    @Synchronized
    override fun onGGA(time: Long, latitude: Double, longitude: Double, altitude: Float, quality: BasicNMEAHandler.FixQuality, satellites: Int, hdop: Float) {
        newLocation(time)

        location!!.latitude = latitude
        location!!.longitude = longitude
        location!!.altitude = altitude.toDouble()
        location!!.accuracy = hdop * 4.0f

        yieldLocation(time, FLAG_GGA)
    }

    @Synchronized
    override fun onGSV(satellites: Int, index: Int, prn: Int, elevation: Float, azimuth: Float, snr: Int) {
        newSatellite(index, satellites, prn, elevation, azimuth, snr)
        yieldSatellites()
    }

    override fun onGSA(type: BasicNMEAHandler.FixType, prns: Set<Int>, pdop: Float, hdop: Float, vdop: Float) {
        activeSatellites = prns

        yieldSatellites()
    }

    @Synchronized
    override fun onUnrecognized(sentence: String) {
        handler!!.onUnrecognized(sentence)
    }

    @Synchronized
    override fun onBadChecksum(expected: Int, actual: Int) {
        handler!!.onBadChecksum(expected, actual)
    }

    @Synchronized
    override fun onException(e: Exception) {
        handler!!.onException(e)
    }

    @Synchronized
    override fun onFinished() {
        handler!!.onFinish()
    }

    companion object {
        val LOCATION_PROVIDER_NAME = "nmea-parser"
        private val FLAG_RMC = 1
        private val FLAG_GGA = 2
        private val LOCATION_FLAGS = FLAG_RMC or FLAG_GGA
        private val SATELLITES_COUNT = 24
    }
}
