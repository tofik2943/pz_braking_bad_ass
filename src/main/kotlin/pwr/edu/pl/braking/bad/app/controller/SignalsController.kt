package pwr.edu.pl.braking.bad.app.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pwr.edu.pl.braking.bad.app.service.SignalService

@RestController
@RequestMapping("/signal")
class SignalsController(@field:Autowired val signalService: SignalService) {

    @PostMapping
    fun addSignal(isStopSignal: Boolean) {
        this.signalService.saveSignal(isStopSignal)
    }
}
