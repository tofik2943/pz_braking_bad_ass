package pwr.edu.pl.braking.bad.app.model.nmea

import android.location.Location

abstract class LocationFactory {
    abstract fun newLocation(): Location
}
